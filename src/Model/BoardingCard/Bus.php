<?php

namespace BoardingCardLibrary\Model\BoardingCard;

/**
 * Class Bus
 *
 * @package BoardingCardLibrary\Model\BoardingCard
 */
class Bus extends BoardingCardFactory
{
    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            "from"      => $this->from,
            "to"        => $this->to,
            "transport" => self::TRAIN_TYPE,
        ];
    }
}
