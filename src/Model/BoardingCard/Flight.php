<?php

namespace BoardingCardLibrary\Model\BoardingCard;

use BoardingCardLibrary\Exception\InvalidJsonContentException;

/**
 * Class Flight
 *
 * @package BoardingCardLibrary\Model\BoardingCard
 */
class Flight extends BoardingCardFactory
{
    /**
     * @var string|null
     */
    private ?string $ticketCounter;

    /**
     * @var string|null
     */
    private ?string $gate;

    /**
     * Flight constructor.
     *
     * @param array $boardingCard
     *
     * @throws InvalidJsonContentException
     */
    public function __construct(array $boardingCard)
    {
        parent::__construct($boardingCard);

        if (!isset($boardingCard['gate'])) {
            throw new InvalidJsonContentException(
                'Each flight\'s boarding card must contains gate'
            );
        }

        $this->gate = $boardingCard['gate'];
        $this->ticketCounter = isset($boardingCard['ticketCounter']) ? $boardingCard['ticketCounter'] : null ;
    }

    /**
     * @return string|null
     */
    public function getGate(): ?string
    {
        return $this->gate;
    }

    /**
     * @return string|null
     */
    public function getTicketCounter(): ?string
    {
        return $this->ticketCounter;
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            "from"              => $this->from,
            "to"                => $this->to,
            "seat"              => $this->seat,
            "transportNumber"   => $this->transportNumber,
            "ticketCounter"     => $this->ticketCounter,
            "gate"              => $this->gate,
            "transport"         => self::FLIGHT_TYPE,
        ];
    }
}
