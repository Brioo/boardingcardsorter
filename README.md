## Boarding Card Sorter:

The goal of this library is to sort a given unordered list of boarding cards (json format), and order it to from and to destination. 
#
#### How to test (without requiring the library trough composer)
* Run:
```
$ cd /var/www/ && git clone https://Brioo@bitbucket.org/Brioo/boardingcardsorter.git && cd boardingcardsorter && touch test.php
```
* Add the following content, to test.php:
```
<?php

require __DIR__ . '/vendor/autoload.php';

use BoardingCardLibrary\Client;

// Given an unordered list of boarding card
// You can add an intermediate step, but should keep the same format regarding the transport
// We use a php array for readability, but we will json_encode it just before
$boardingCards = [
    [
        "transport" => 'bus',
        "from" => 'Barcelona',
        "to" => 'Gerona Airport',
    ],
    [
        "transport" => 'flight',
        "from" => 'Gerona Airport',
        "to" => 'Stockholm',
        "transportNumber" => 'SK455',
        "gate" => '45B',
        "seat" => '3A',
        "ticketCounter" => null,
    ],
    [
        "transport" => 'flight',
        "from" => 'Stockholm',
        "to" => 'New York JFK',
        "transportNumber" => 'SK22',
        "gate" => '22',
        "seat" => '7B',
        "ticketCounter" => '344',
    ],
    [
        "transport" => 'train',
        "from" => 'Madrid',
        "to" => 'Barcelona',
        "transportNumber" => '78A',
        "seat" => '45B',
    ],
];

try {
    $boardingCardSorterClient = new Client(json_encode($boardingCards));

    echo $boardingCardSorterClient->sortBoardingCards(Client::OUTPUT_FORMAT_READABLE);
} catch (Exception $e) {
    echo "[BoardingCardLibrary] " . $e->getMessage();
    die;
}

```
Run to see:
```
$ composer install
$ php test.php
```

You can change output format with `Client::OUTPUT_FORMAT_READABLE `or `Client::OUTPUT_FORMAT_JSON`
#



###How to install (use as a real library):
* Require the library with the url of the repository, within your composer.json:
* Run composer

```
  "repositories": [
    {
      "type": "vcs",
      "url": "https://Brioo@bitbucket.org/Brioo/boardingcardsorter.git"
    }
  ]
```

There is no release for now, so you must specify master branch with *dev-master*
```
$ composer require quentinbens/boarding-card-sorter dev-master
```

Check the test.php example above to use it as you need. 
