<?php

namespace BoardingCardLibrary\Model\BoardingCard;

use BoardingCardLibrary\Exception\InvalidJsonContentException;

/**
 * Class BoardingCardFactory
 *
 * @package BoardingCardLibrary\Model\BoardingCard
 */
abstract class BoardingCardFactory implements BoardingCardFactoryInterface
{
    /**
     * All available boarding card type
     */
    public const FLIGHT_TYPE    = "flight";
    public const BUS_TYPE       = "bus";
    public const TRAIN_TYPE     = "train";

    public const BOARDING_CARD_TYPES = [
        self::FLIGHT_TYPE,
        self::BUS_TYPE,
        self::TRAIN_TYPE,
    ];

    /**
     * @var string|null $seat
     */
    protected ?string $seat;

    /**
     * @var string $from
     */
    protected string $from;

    /**
     * @var string $to
     */
    protected string $to;

    /**
     * @var string $transportNumber
     */
    protected ?string $transportNumber;

    /**
     * BoardingCardFactory constructor.
     *
     * @param array $boardingCard
     *
     * @throws InvalidJsonContentException
     */
    public function __construct(array $boardingCard)
    {
        if (!isset($boardingCard['from'])
            || !isset($boardingCard['to'])
        ) {
            throw new InvalidJsonContentException('Each boarding card must contains from and to');
        }

        $this->from = $boardingCard['from'];
        $this->to = $boardingCard['to'];

        $this->transportNumber = isset($boardingCard['transportNumber']) ? $boardingCard['transportNumber'] : null;
        $this->seat = isset($boardingCard['seat']) ? $boardingCard['seat'] : null;
    }

    /**
     * @return string|null
     */
    public function getSeat(): ?string
    {
        return $this->seat;
    }

    /**
     * @param string|null $seat
     *
     * @return $this
     */
    public function setSeat($seat)
    {
        $this->seat = $seat;

        return $this;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     *
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     *
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransportNumber(): string
    {
        return $this->transportNumber;
    }

    /**
     * @param string $transportNumber
     *
     * @return $this
     */
    public function setTransportNumber($transportNumber)
    {
        $this->transportNumber = $transportNumber;

        return $this;
    }

    /**
     * @return string
     *
     * @throws \ReflectionException
     */
    public function getTransport()
    {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }
}
