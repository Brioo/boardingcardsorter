<?php


namespace BoardingCardLibrary;

use BoardingCardLibrary\Exception\InvalidFormatException;
use BoardingCardLibrary\Exception\InvalidJsonContentException;
use BoardingCardLibrary\Model\BoardingCard\BoardingCardFactory;
use BoardingCardLibrary\Model\BoardingCard\Bus;
use BoardingCardLibrary\Model\BoardingCard\Flight;
use BoardingCardLibrary\Model\BoardingCard\Train;
use http\Exception\InvalidArgumentException;

/**
 * Class Client
 *
 * @package BoardingCardLibrary
 */
class Client
{
    /**
     * Public const below
     */
    public const OUTPUT_FORMAT_JSON = "json";
    public const OUTPUT_FORMAT_READABLE = "readable";


    /**
     * @var BoardingCardFactory[]|array $boardingCards
     */
    private $boardingCards = [];

    /**
     * Client constructor.
     *
     * @param string $boardingCards
     *
     * @throws InvalidFormatException
     */
    public function __construct(string $boardingCards)
    {
        if (!$this->isJson($boardingCards)) {
            throw new InvalidFormatException();
        }

        $this->boardingCards = json_decode($boardingCards, true);
    }

    /**
     * @param string $outputFormat
     *
     * @return string
     *
     * @throws InvalidJsonContentException
     */
    public function sortBoardingCards(string $outputFormat = self::OUTPUT_FORMAT_READABLE)
    {
        $boardingCards = [];

        // Generate boarding cards
        foreach ($this->boardingCards as $boardingCard) {
            if (!isset($boardingCard['transport'])) {
                throw  new InvalidJsonContentException();
            }

            switch ($boardingCard['transport']) {
                case BoardingCardFactory::BUS_TYPE:
                    $boardingCards[] = new Bus($boardingCard);
                    break;
                case BoardingCardFactory::TRAIN_TYPE:
                    $boardingCards[] = new Train($boardingCard);
                    break;
                case BoardingCardFactory::FLIGHT_TYPE:
                    $boardingCards[] = new Flight($boardingCard);
                    break;
                default:
                    $message = sprintf(
                        "The Json input must contains the 'transport' type of each boarding cards: %s",
                        implode(", ", BoardingCardFactory::BOARDING_CARD_TYPES)
                    );

                    new InvalidJsonContentException($message);
            }
        }

        // Sort boarding cards in good order
        usort($boardingCards, fn($a, $b) => self::sortByDestination($b, $a));
        $this->boardingCards = $boardingCards;

        return $this->output($outputFormat);
    }

    /**
     * @param string $outputFormat
     *
     * @throws \Exception
     */
    public function output(string $outputFormat)
    {
        switch ($outputFormat) {
            case self::OUTPUT_FORMAT_JSON:
                return $this->formatToJson();
            case self::OUTPUT_FORMAT_READABLE:
                echo $this->formatToReadableText();
                die;
        }

        throw new \Exception('Invalid output format parameter');
    }

    /**
     * @return false|string
     */
    private function formatToJson(): string
    {
        $output = [];

        foreach ($this->boardingCards as $boardingCard) {
            $output[] = $boardingCard->serialize();
        }

        return json_encode($output);
    }

    /**
     * @return string
     *
     * @throws \ReflectionException
     */
    private function formatToReadableText(): string
    {
        $output = "";
        $lastElement = count($this->boardingCards);

        foreach ($this->boardingCards as $key => $boardingCard) {
            switch ($boardingCard->getTransport()) {
                case BoardingCardFactory::FLIGHT_TYPE:
                    /** @var Flight $boardingCard */
                    $output .= "From {$boardingCard->getFrom()}, "
                        . "take flight {$boardingCard->getTransportNumber()} "
                        . "to {$boardingCard->getTo()}, "
                        . "Gate {$boardingCard->getGate()}, "
                        . "seat {$boardingCard->getSeat()}. ";

                    if (is_null($boardingCard->getTicketCounter())) {
                        $output .= "Baggage drop at ticket counter {$boardingCard->getTicketCounter()}." . PHP_EOL;
                    } else {
                        $output .= "Baggage will we automatically transferred from your last leg." . PHP_EOL;
                    }
                    break;
                case BoardingCardFactory::TRAIN_TYPE:
                    /** @var Train $boardingCard */
                    $output .= "Take train {$boardingCard->getTransportNumber()} "
                        . "from {$boardingCard->getFrom()} "
                        . "to {$boardingCard->getTo()}. "
                        . "Sit in seat {$boardingCard->getSeat()}. "
                        . PHP_EOL;
                    break;
                case BoardingCardFactory::BUS_TYPE:
                    /** @var Bus $boardingCard */
                    $output .= "Take airport bus from {$boardingCard->getFrom()} "
                        . "to {$boardingCard->getTo()}. ";

                    if (!is_null($boardingCard->getSeat())) {
                        $output .= "Sit in seat {$boardingCard->getSeat()}." . PHP_EOL;
                    } else {
                        $output .= "No seat assignment." . PHP_EOL;
                    }
                    break;
            }

            if ($key + 1 == $lastElement) {
                $output .= "You have arrived at your final destination." . PHP_EOL;
            }
        }

        return $output;
    }

    /**
     * @param BoardingCardFactory string $a
     * @param BoardingCardFactory string $b
     *
     * @return int
     */
    private static function sortByDestination(BoardingCardFactory $a, BoardingCardFactory $b)
    {
        if (strtolower($a->getFrom()) == strtolower($b->getTo())) {
            return -1;
        }

        return 1;
    }

    /**
     * @param string $string
     *
     * @return bool
     */
    private function isJson(string $string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }
}
