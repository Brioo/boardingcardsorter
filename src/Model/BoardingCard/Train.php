<?php

namespace BoardingCardLibrary\Model\BoardingCard;

/**
 * Class Train
 *
 * @package BoardingCardLibrary\Model\BoardingCard
 */
class Train extends BoardingCardFactory
{
    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            "from"              => $this->from,
            "to"                => $this->to,
            "transportNumber"   => $this->transportNumber,
            "seat"              => $this->seat,
            "transport"         => self::TRAIN_TYPE,
        ];
    }
}
