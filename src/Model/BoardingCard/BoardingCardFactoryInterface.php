<?php

namespace BoardingCardLibrary\Model\BoardingCard;

/**
 * Class BoardingCardFactoryInterface
 *
 * @package BoardingCardLibrary\Model\BoardingCard
 */
interface BoardingCardFactoryInterface
{
    /**
     * @return array
     */
    public function serialize();

    /**
     * @return string
     */
    public function getTransport();
}
