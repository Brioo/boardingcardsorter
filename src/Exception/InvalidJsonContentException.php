<?php

namespace BoardingCardLibrary\Exception;

use Throwable;

/**
 * Class InvalidFormatException
 *
 * @package BoardingCardLibrary\Exception
 */
class InvalidJsonContentException extends \Exception
{
    /**
     * InvalidFormatException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
